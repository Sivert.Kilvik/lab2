package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private final int maxInFridge = 20;
    private List<FridgeItem> register;

    public Fridge(){ // constructor
        this.register = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return register.size();
    }

    @Override
    public int totalSize() {
        return maxInFridge;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (totalSize() > nItemsInFridge()){
            return register.add(item);
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (register.contains(item)) {
            register.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        register.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList();
        for (FridgeItem items : register){
            if  (items.hasExpired()){
                expiredFood.add(items);
            }
        }
        for (FridgeItem items : expiredFood){
            register.remove(items);
        }
        return expiredFood;
    }

}
